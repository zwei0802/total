//
//  LTDownloadOperation.h
//  Total
//
//  Created by xin on 2017/6/27.
//  Copyright © 2017年 elephants. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol  LTDownloadOperationDelegate <NSObject>

- (void)downloadFinishWithImage:(UIImage *)image;

@end

@interface LTDownloadOperation : NSOperation

@property (nonatomic, copy) NSString * imageUrl;

@property (nonatomic, weak) id <LTDownloadOperationDelegate> delegate;


- (id)initWithUrl:(NSString *)url delegate:(id<LTDownloadOperationDelegate>)delegate;

@end
