//
//  TLanguageManager.h
//  Total
//
//  Created by xin on 2017/6/28.
//  Copyright © 2017年 elephants. All rights reserved.
//
/*
 
 需要在项目中设置多语言，在project的localizations内添加支持的语言。
 然后新建一个strings文件，在里面写上不同语言的key对应的值。
 使用了一个plist表来展示支持语种的数据源。 language.plist
 
 */

#import <Foundation/Foundation.h>

@class TLanguageEntity;

#define TStringWithKeyFromTable(key, tbl) [[TLanguageManager sharedManager] getStringForKey:key withTable:tbl]

FOUNDATION_EXPORT NSString * const TLanguageChangeNotification;

@interface TLanguageManager : NSObject
//TLanguageEntity对象的数组，可以通过这个数组来进行显示
@property (nonatomic,strong) NSArray * languageList;

//单例实现
+ (id)sharedManager;

//返回table中指定的key的值
- (NSString *)getStringForKey:(NSString *)key withTable:(NSString *)table;

//改变语言
- (void)changeLanguage:(TLanguageEntity *)entity;



@end
