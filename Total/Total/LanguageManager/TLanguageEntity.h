//
//  TLanguageEntity.h
//  Total
//
//  Created by xin on 2017/6/29.
//  Copyright © 2017年 elephants. All rights reserved.
//  引入了MJExtension为了支持nscoding协议

#import <Foundation/Foundation.h>
#import <MJExtension/MJExtension.h>

@interface TLanguageEntity : NSObject
//语言名称
@property (nonatomic,copy) NSString * languageName;
//语言文件本地地址
@property (nonatomic,copy) NSString * languageFilePath;

@end
