//
//  LocalizationController.m
//  Total
//
//  Created by xin on 2017/6/29.
//  Copyright © 2017年 elephants. All rights reserved.
//

#import "LocalizationController.h"
#import "TLanguageManager.h"
#import "TLanguageEntity.h"

@interface LocalizationController ()
{
    
    __weak IBOutlet UILabel *label1;
    __weak IBOutlet UILabel *label2;
    __weak IBOutlet UILabel *label3;
    __weak IBOutlet UILabel *label4;
    __weak IBOutlet UILabel *label5;
}
@end

@implementation LocalizationController

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshUIText:)
                                                 name:TLanguageChangeNotification
                                               object:nil];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TLanguageChangeNotification object:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setText];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemBookmarks target:self action:@selector(changeLanguage:)];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)changeLanguage:(id)sender
{
    UIAlertController * alterController = [UIAlertController alertControllerWithTitle:TStringWithKeyFromTable(@"appLanguage",@"CustomLocalizable") message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    NSArray * list = [[TLanguageManager sharedManager] languageList];
    
    for (TLanguageEntity * entity in list) {
        UIAlertAction * enAction = [UIAlertAction actionWithTitle:entity.languageName style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            //转换语言
            [[TLanguageManager sharedManager] changeLanguage:entity];
        }];
        
        [alterController addAction:enAction];
    }
    
    UIAlertAction * cancelAction = [UIAlertAction actionWithTitle:TStringWithKeyFromTable(@"appCancel",@"CustomLocalizable") style:UIAlertActionStyleCancel handler:nil];
    [alterController addAction:cancelAction];
    
    [self presentViewController:alterController animated:YES completion:nil];
}

- (void)refreshUIText:(id)sender
{
    NSLog(@"%s",__func__);
    [self setText];
    
}

- (void)setText
{
    label1.text = TStringWithKeyFromTable(@"tabarCommerce",@"CustomLocalizable");
    label2.text = TStringWithKeyFromTable(@"tabarUnion",@"CustomLocalizable");
    label3.text = TStringWithKeyFromTable(@"tabarAddressBook",@"CustomLocalizable");
    label4.text = TStringWithKeyFromTable(@"tabarMessage",@"CustomLocalizable");
    label5.text = TStringWithKeyFromTable(@"tabarMy",@"CustomLocalizable");
}

- (void)dealloc
{
    NSLog(@"%s",__func__);
}

@end
