//
//  LTDownloadProgress.h
//  Total
//
//  Created by xin on 2017/8/4.
//  Copyright © 2017年 elephants. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LTDownloadProgress : NSObject

- (nullable instancetype)initWithDownloadProgress:(float)aDownloadProgress
                                 expectedFileSize:(int64_t)anExpectedFileSize
                                 receivedFileSize:(int64_t)anReceivedFileSize
                           estimatedRemainingTime:(NSTimeInterval)anEstimedRemainingTime
                              bytesPerSecondSpeed:(NSUInteger)aBytesSecondSpeed
                                         progress:(nonnull NSProgress *)aProgress;
- (nullable instancetype)init __attribute__((unavailable("use initWithDownloadProgress:expectedFileSize:receivedFileSize:estimatedRemainingTime:bytesPerSecondSpeed:progress:")));
+ (nullable instancetype)new __attribute__((unavailable("use initWithDownloadProgress:expectedFileSize:receivedFileSize:estimatedRemainingTime:bytesPerSecondSpeed:progress:")));

@property (nonatomic, assign, readonly) float downloadProgress;
@property (nonatomic, assign, readonly) int64_t expectedFileSize;
@property (nonatomic, assign, readonly) int64_t receivedFileSize;
@property (nonatomic, assign, readonly) NSTimeInterval estimatedRemainingTime;
@property (nonatomic, assign, readonly) NSUInteger bytesPerSecondSpeed;
@property (nonatomic, strong, readonly, nonnull) NSProgress *nativeProgress;
@property (nonatomic, strong, readwrite, nullable) NSString *lastLocalizedDescription;
@property (nonatomic, strong, readwrite, nullable) NSString *lastLocalizedAdditionalDescription;

@end
