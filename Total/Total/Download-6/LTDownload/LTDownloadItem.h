//
//  LTDownloadItem.h
//  Total
//
//  Created by xin on 2017/7/28.
//  Copyright © 2017年 elephants. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 LTDownloadItem 是在LTDownloadManager类中内部使用，用来管理每一条下载任务的类
 */
@interface LTDownloadItem : NSObject

- (nullable instancetype)initWithDownloadToken:(nonnull NSString *)downloadToken sessionDownloadTask:(nullable NSURLSessionDownloadTask *)aSessionDownloadTask;

@property (nonatomic, strong, nullable) NSDate *downloadStartDate;
@property (nonatomic, assign) int64_t receivedFileSizeInBytes;
@property (nonatomic, assign) int64_t expectedFileSizeInBytes;
@property (nonatomic, assign) int64_t resumedFileSizeInBytes;
@property (nonatomic, assign) NSInteger bytesPerSecondSpeed;
@property (nonatomic, strong, readonly, nonnull) NSProgress *progress;
@property (nonatomic, strong, readonly, nonnull) NSString * downloadToken;
@property (nonatomic, strong, readonly, nullable) NSURLSessionDownloadTask * sessionDownloadTask;
@property (nonatomic, strong, nullable) NSArray<NSString *> *errorMessagesStack;
@property (nonatomic, assign) NSInteger lastHttpStatusCode;
@property (nonatomic, strong, nullable) NSURL *finalLocalFileURL;

- (nullable LTDownloadItem *)init __attribute__((unavailable("use initWithDownloadToken:sessionDownloadTask:")));

+ (nullable LTDownloadItem *)new __attribute__((unavailable("use initWithDownloadToken:sessionDownloadTask:")));

@end
