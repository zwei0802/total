//
//  LTDownloadDelegate.h
//  Total
//
//  Created by xin on 2017/7/27.
//  Copyright © 2017年 elephants. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol LTDownloadDelegate <NSObject>


/**
 当一个下载完成时，会调用该方法进行通知。
 @param downloadIdentifier LTDownloadItem的下载标识符。
 @param localFileURL 本地文件保存所在目录。
 @discussion localFileURL是绝对路径，如果需要归档使用，只取lastPathComponent来保存，在使用时拼接上LTDownloadManager的文件下载路径才能使用。
 */
- (void)downloadDidCompleteWithIdentifier:(nonnull NSString *)downloadIdentifier localFileURL:(nonnull NSURL *)localFileURL;

/**
 下载失败的通知。

 @param downloadIdentifier LTDownloadItem的下载标识符。
 @param error 错误描述。
 @param httpStatusCode http错误编码。
 @param errorMessageStack 错误信息数组。
 @param resumaData 不完全的下载数据，可以重用后是否再次开始下载。
 */
- (void)downloadFailedWithIdentifier:(nonnull NSString *)downloadIdentifier
                               error:(nonnull NSError *)error
                      httpStatusCode:(NSInteger)httpStatusCode
                  errorMessagesStack:(nullable NSArray<NSString *> *)errorMessageStack
                          resumeData:(nullable NSData *)resumaData;

/**
 下载开始时通知网络活动指示器的显示(左上角信号栏边上的网络活动指示器的显示)。
 在UIApplication's中设置setNetworkActivityIndicatorVisible: 来显示在导航栏的网络下载提示,默认是不显示，需要用户在方法中设置。
 */
- (void)incrementNetworkActivityIndicatorActivityCount;


/**
 网络活动停止时调用该方法停止网络活动指示器的显示
 */
- (void)decrementNetworkActivityIndicatorActivityCount;


@optional


/**
 当一个下载进度有改变调用此方法

 @param downloadIdentifier 是下载任务的taskDescription。
 */
- (void)downloadProgressChangedForIdentifier:(nonnull NSString *)downloadIdentifier;

/**
 当一个下载暂停时调用此方法

 @param downloadIdentifier LTDownloadItem的下载标识符。
 @param resumaData 不完全的下载数据，可以重用后是否再次开始下载。
 */
- (void)downloadPausedWithIdentifier:(nonnull NSString *)downloadIdentifier resumaData:(nullable NSData *)resumaData;

/**
 当一个下载恢复时调用此方法，用于更新当天下载的状态

 @param downloadIdentifer LTDownloadItem的下载标识符。
 */
- (void)resumeDownloadWithIdentifier:(nonnull NSString *)downloadIdentifer;

/**
 设置下载完成时，本地存储文件的路径，如果不实现该方法，默认保存在document目录下的file-download文件夹内。

 @param downloadIdentifer 下载完成的LTDownloadItem的下载标识符。
 @param remoteURL 下载的远程地址。
 */
- (nullable NSURL *)localFileURLForIdentifier:(nonnull NSString *)downloadIdentifer remoteURL:(nonnull NSURL *)remoteURL;

/**
 验证下载数据调用此方法

 @param localFileURL 本地文件路径
 @param downloadIdentifier LTDownloadItem的下载标识符。
 @return 如果本地下载数据通过验证返回TRUE
 @discussion 一条下载任务可能成功完成带着一个错误的下载数据字符串。这个方法是用来检查下载的数据是否是期望的内容和数据类型。如果没有实现此方法，每个下载都是有效的。
 */
- (BOOL)downloadAtLocationFileURL:(nonnull NSURL *)localFileURL isValidForDownloadIdentifier:(nonnull NSString*)downloadIdentifier;

/**
 验证http状态编码

 @param httpStatusCode http响应的状态编码
 @param downloadIdentifier LTDownloadItem的下载标识符。
 @return 如果http状态编码的值是正确的返回TURE
 */
- (BOOL)httpStatusCode:(NSInteger)httpStatusCode isVailDownloadIdentifier:(nonnull NSString *)downloadIdentifier;

/**
 返回自定义的后台回话配置，使用者可以在此修改配置，只在初始化的时候调用一次。

 @param backgroundSessionConfiguraion 后台会话配置
 */
- (void)customizeBackgroundSessionConfiguration:(nonnull NSURLSessionConfiguration *)backgroundSessionConfiguraion;

/**
  总进度对象，是所有下载任务进度的集合进度对象。每一个下载任务都是他的子集。

 @return 进度对象
 */
- (nullable NSProgress *)rootProgress;

@end
