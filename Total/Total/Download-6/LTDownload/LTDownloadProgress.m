//
//  LTDownloadProgress.m
//  Total
//
//  Created by xin on 2017/8/4.
//  Copyright © 2017年 elephants. All rights reserved.
//

#import "LTDownloadProgress.h"

@interface LTDownloadProgress() <NSCoding>

@property (nonatomic, assign, readwrite) float downloadProgress;
@property (nonatomic, assign, readwrite) int64_t expectedFileSize;
@property (nonatomic, assign, readwrite) int64_t receivedFileSize;
@property (nonatomic, assign, readwrite) NSTimeInterval estimatedRemainingTime;
@property (nonatomic, assign, readwrite) NSUInteger bytesPerSecondSpeed;
@property (nonatomic, strong, readwrite, nonnull) NSProgress *nativeProgress;

@end

@implementation LTDownloadProgress

#pragma mark Initalization

- (nullable instancetype) initWithDownloadProgress:(float)aDownloadProgress expectedFileSize:(int64_t)anExpectedFileSize receivedFileSize:(int64_t)anReceivedFileSize estimatedRemainingTime:(NSTimeInterval)anEstimedRemainingTime bytesPerSecondSpeed:(NSUInteger)aBytesSecondSpeed progress:(NSProgress *)aProgress
{
    self = [super init];
    if (self)
    {
        self.downloadProgress = aDownloadProgress;
        self.expectedFileSize = anExpectedFileSize;
        self.receivedFileSize = anReceivedFileSize;
        self.estimatedRemainingTime = anEstimedRemainingTime;
        self.bytesPerSecondSpeed = aBytesSecondSpeed;
        self.nativeProgress = aProgress;
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:@(self.downloadProgress) forKey:@"downloadProgress"];
    [aCoder encodeObject:@(self.expectedFileSize) forKey:@"expectedFileSize"];
    [aCoder encodeObject:@(self.receivedFileSize) forKey:@"receivedFileSize"];
    [aCoder encodeObject:@(self.estimatedRemainingTime) forKey:@"estimatedRemainingTime"];
    [aCoder encodeObject:@(self.bytesPerSecondSpeed) forKey:@"bytesPerSecondSpeed"];
    if (self.lastLocalizedDescription) {
        [aCoder encodeObject:self.lastLocalizedDescription forKey:@"lastLocalizedDescription"];
    }
    if (self.lastLocalizedAdditionalDescription) {
        [aCoder encodeObject:self.lastLocalizedAdditionalDescription forKey:@"lastLocalizedAdditionalDescription"];
    }
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        self.downloadProgress = [[aDecoder decodeObjectForKey:@"downloadProgress"] floatValue];
        self.expectedFileSize = (int64_t)[aDecoder decodeObjectForKey:@"expectedFileSize"];
        self.receivedFileSize = (int64_t)[aDecoder decodeObjectForKey:@"receivedFileSize"];
        self.estimatedRemainingTime = (NSTimeInterval)[[aDecoder decodeObjectForKey:@"estimatedRemainingTime"] doubleValue];
        self.bytesPerSecondSpeed = [[aDecoder decodeObjectForKey:@"bytesPerSecondSpeed"] unsignedIntegerValue];
        self.lastLocalizedDescription = [aDecoder decodeObjectForKey:@"lastLocalizedDescription"];
        self.lastLocalizedAdditionalDescription = [aDecoder decodeObjectForKey:@"lastLocalizedAdditionalDescription"];
    }
    return self;
}

#pragma mark - Description

- (nonnull NSString *)description
{
    NSMutableDictionary *aDescriptionDict = [NSMutableDictionary dictionary];
    
    [aDescriptionDict setObject:@(self.downloadProgress) forKey:@"downloadProgress"];
    [aDescriptionDict setObject:@(self.expectedFileSize) forKey:@"expectedFileSize"];
    [aDescriptionDict setObject:@(self.receivedFileSize) forKey:@"receivedFileSize"];
    [aDescriptionDict setObject:@(self.estimatedRemainingTime) forKey:@"estimatedRemainingTime"];
    [aDescriptionDict setObject:@(self.bytesPerSecondSpeed) forKey:@"bytesPerSecondSpeed"];
    [aDescriptionDict setObject:self.nativeProgress forKey:@"nativeProgress"];
    if (self.lastLocalizedAdditionalDescription) {
        [aDescriptionDict setObject:self.lastLocalizedAdditionalDescription forKey:@"lastLocalizedAdditionalDescription"];
    }
    if (self.lastLocalizedDescription) {
        [aDescriptionDict setObject:self.lastLocalizedDescription forKey:@"lastLocalizedDescription"];
    }
    NSString * aDescripotionString = [NSString stringWithFormat:@"%@",aDescriptionDict];
    return aDescripotionString;
}

@end
