//
//  LTDownloadManager.h
//  Total
//
//  Created by xin on 2017/7/24.
//  Copyright © 2017年 elephants. All rights reserved.
/*
 只支持NSURLSession下载，所有类库支持iOS7以上
 */

#import <Foundation/Foundation.h>

#import "LTDownloadDelegate.h"
#import "LTBackgroundSessionCompletionHandlerBlock.h"
#import "LTDownloadProgress.h"

/**
 LTDownloadManagerPauseResumaDataBlock 是一个可选的块用来存储取消下载时的可恢复数据。

 @param resumeData 下载继续下载的可恢复数据。
 */
typedef void (^LTDownloadManagerPauseResumaDataBlock)(NSData * _Nullable resumeData);

@interface LTDownloadManager : NSObject

/**
 NSURLSession的configuration标识符
 */
@property (readonly, nonatomic, copy, nonnull) NSString *backgroundSessionIdentifier;

#pragma mark - Initialization

- (nullable instancetype)initWithDelegate:(nonnull NSObject<LTDownloadDelegate>*)delegate;

- (nullable instancetype)initWithDelegate:(nonnull NSObject<LTDownloadDelegate>*)delegate maxConcurrentDownloads:(NSInteger)maxConcurrentFileDownloadsCount;

- (nullable instancetype)initWithDelegate:(nonnull NSObject<LTDownloadDelegate>*)delegate maxConcurrentDownloads:(NSInteger)maxConcurrentFileDownloadsCount backgroundSessionIdentifier:(nonnull NSString *)backgroundSessionIdentifier;

- (nullable LTDownloadManager *)init __attribute__((unavailable("use initWithDelegate:maxConcurrentDownloads: or initWithDelegate:")));

+ (nullable LTDownloadManager *)new __attribute__((unavailable("use initWithDelegate:maxConcurrentDownloads: or initWithDelegate:")));

/**
 Set up file downloader.

 @param aSetupCompletionBlock Completion block to be called asynchronously after setup is finished.
 */
- (void)setupWithCompletion:(nullable void (^)(void))aSetupCompletionBlock;


#pragma mark - Download


/**
 开始一个下载任务。

 @param aDownloadIdentifier LTDownloadItem的downloadToken，是下载标识符。
 @param aRemoteURL 远程下载地址。
 */
- (void)startDownloadWithIdentifier:(nonnull NSString *)aDownloadIdentifier fromRemoteURL:(nonnull NSURL *)aRemoteURL;

/**
 开始一个下载任务。

 @param aDownloadIdentifier LTDownloadItem的downloadToken，是下载标识符。
 @param aResumeData 暂停下载时得到的可恢复数据，用于继续下载。
 */
- (void)startDownloadWithIdentifier:(nonnull NSString *)aDownloadIdentifier usingResumeData:(nonnull NSData *)aResumeData;

/**
 查询当前下载ID是否为有效下载
 
 @param aDownloadIdentifier 下载标示ID
 @return 有效下载判断
 */
- (BOOL)isDownloadingIdentifier:(nonnull NSString *)aDownloadIdentifier;

/**
 返回一个下载任务是否是等待开始。

 @param aDownloadIdentifier LTDownloadItem的downloadToken，是下载标识符。
 @return 如果一个下载任务是等待下载返回YES，否则返回NO。
 */
- (BOOL)isWaitingForDownloadOfIdentifier:(nonnull NSString *)aDownloadIdentifier;

/**
 查询当前是否有正在下载的任务。

 @return 如果有正在下载的任务返回YES。
 */
- (BOOL)hasActiveDownloads;

/**
 取消一下下载任务。

 @param aDownloadIdentifier LTDownloadItem的downloadToken，是下载标识符。
 */
- (void)cancelDownloadWithIdentifier:(nonnull NSString *)aDownloadIdentifier;

#pragma mark - BackgroundSessionCompletionHandler


/**
 设置后台会话完成的处理程序块，用于存储从APPDelegate方法中从回台回来的块，在session都初始化完毕后调用该块可获得session delegate的回调，获得在后台下载的数据信息来更新处理。
 @param aBackgroundSessionCompletionHandlerBlock 后台会话完成回调block。
 */
- (void)setBackgroundSessionCompletionHandlerBlock:(nullable LTBackgroundSessionCompletionHandlerBlock)aBackgroundSessionCompletionHandlerBlock;

#pragma mark - Progress

- (nullable LTDownloadProgress *)downloadProgressForIdentifier:(nonnull NSString *)aDownloadIdentifier;

@end
