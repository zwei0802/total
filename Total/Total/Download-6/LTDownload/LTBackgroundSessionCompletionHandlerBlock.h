//
//  LTBackgroundSessionCompletionHandlerBlock.h
//  Total
//
//  Created by xin on 2017/7/28.
//  Copyright © 2017年 elephants. All rights reserved.
//

#ifndef LTBackgroundSessionCompletionHandlerBlock_h
#define LTBackgroundSessionCompletionHandlerBlock_h

typedef void (^LTBackgroundSessionCompletionHandlerBlock)();


#endif /* LTBackgroundSessionCompletionHandlerBlock_h */
