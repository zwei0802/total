//
//  Person+Associated.m
//  Total
//
//  Created by xin on 2017/7/8.
//  Copyright © 2017年 elephants. All rights reserved.
//

#import "Person+Associated.h"
#import "LTAssociated.h"

@implementation Person (Associated)

ASSOCIATED(title, setTitle, NSString *, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
ASSOCIATED_BOOL(sex, setSex);
ASSOCIATED_NSInteger(age, setAge);

@end
