//
//  Person.m
//  Total
//
//  Created by xin on 2017/7/7.
//  Copyright © 2017年 elephants. All rights reserved.
//

#import "Person.h"

@implementation Person

/**
 *吃饭实例方法  无参数 无返回值
 */
-(void)eat{
    NSLog(@"eat_person");
}
/**
 *吃饭类方法  无参数  无返回值
 */
+(void)eat{
    NSLog(@"eat_class");
}

/**
 *睡觉实例方法  有参数 无返回值
 */
-(void)sleepOfHour:(NSNumber*)hour{
    NSLog(@"sleep_person_%@",hour);
}
/**
 *睡觉类方法  有参数 无返回值
 */
+(void)sleepOfHour:(NSNumber*)hour{
    NSLog(@"sleep_class_%@",hour);
}


/**
 *是否吃饱实例方法 有参数 有返回值
 */
-(NSNumber*)eatEnough:(NSNumber*)breadCount{
    NSLog(@"breadCount_person_%@",breadCount);
    return @(1);
}

/**
 *是否吃饱类方法 有参数 有返回值
 */
+(NSNumber*)eatEnough:(NSNumber*)breadCount{
    NSLog(@"breadCount_class_%@",breadCount);
    return @(0);
}

@end
