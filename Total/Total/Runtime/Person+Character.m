//
//  Person+Character.m
//  Total
//
//  Created by xin on 2017/7/8.
//  Copyright © 2017年 elephants. All rights reserved.
//

#import "Person+Character.h"
#import "objc/runtime.h"

@implementation Person (Character)

- (NSString *)name{
    return objc_getAssociatedObject(self, @"name");
}

- (void)setName:(NSString *)name
{
    objc_setAssociatedObject(self, @"name", name, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end
