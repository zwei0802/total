//
//  Person+Character.h
//  Total
//
//  Created by xin on 2017/7/8.
//  Copyright © 2017年 elephants. All rights reserved.
//

#import "Person.h"

@interface Person (Character)

@property (nonatomic ,strong) NSString * name;

@end
