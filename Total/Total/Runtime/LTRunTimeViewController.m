//
//  LTRunTimeViewController.m
//  Total
//
//  Created by xin on 2017/7/7.
//  Copyright © 2017年 elephants. All rights reserved.
//

#import "LTRunTimeViewController.h"
#import "Person.h"
#import "Person+Character.h"
#import "Person+Associated.h"

#import "Father.h"
#import "Son.h"
#import "objc/objc-runtime.h"
#import "NSObject+RunTime.h"

@interface LTRunTimeViewController ()
@property (weak, nonatomic) IBOutlet UITextView *textView;

@end

@implementation LTRunTimeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    Person * person = [Person new];
    NSLog(@"%p",[Person class]);
    NSLog(@"%p",[person class]);
    NSLog(@"%p",object_getClass(person));
    NSLog(@"%p",object_getClass([person class]));
    person.name = @"德玛西亚";
    NSLog(@"=====");
    
    [NSObject msgSendToObj:person Selector:NSSelectorFromString(@"eat") Prarms:nil NeedReturn:NO];
    [NSObject msgSendToClass:NSClassFromString(@"Person") Selector:NSSelectorFromString(@"eat") Prarms:nil NeedReturn:NO];
    
    [NSObject msgSendToObj:person Selector:NSSelectorFromString(@"sleepOfHour:") Prarms:@[@(10)] NeedReturn:NO];
    [NSObject msgSendToClass:NSClassFromString(@"Person") Selector:NSSelectorFromString(@"sleepOfHour:") Prarms:@[@(8)] NeedReturn:NO];
    //
    id objR = [NSObject msgSendToObj:person Selector:NSSelectorFromString(@"eatEnough:") Prarms:@[@(100)] NeedReturn:YES];
    id classR = [NSObject msgSendToClass:NSClassFromString(@"Person") Selector:NSSelectorFromString(@"eatEnough:") Prarms:@[@(100)] NeedReturn:YES];
    NSLog(@"objR_%@ classR_%@", objR, classR);
    NSLog(@"=====");
    //
    [NSObject msgSendToObj_invocation:person Selector:NSSelectorFromString(@"eat") Prarms:nil NeedReturn:NO];
    [NSObject msgSendToClass_invocation:NSClassFromString(@"Person") Selector:NSSelectorFromString(@"eat") Prarms:nil NeedReturn:NO];
    //
    [NSObject msgSendToObj_invocation:person Selector:NSSelectorFromString(@"sleepOfHour:") Prarms:@[@(10)] NeedReturn:NO];
    [NSObject msgSendToClass_invocation:NSClassFromString(@"Person") Selector:NSSelectorFromString(@"sleepOfHour:") Prarms:@[@(8)] NeedReturn:NO];
    //
    id objR_in = [NSObject msgSendToObj_invocation:person Selector:NSSelectorFromString(@"eatEnough:") Prarms:@[@(100)] NeedReturn:YES];
    id classR_in = [NSObject msgSendToClass_invocation:NSClassFromString(@"Person") Selector:NSSelectorFromString(@"eatEnough:") Prarms:@[@(100)] NeedReturn:YES];
    NSLog(@"objR_in_%@ classR_in_%@", objR_in, classR_in);
    NSLog(@"=====");
    
    [NSObject getAllIvarNameWithClass:[UITextView class] Completed:^(NSArray *ivarNameArray) {
        NSLog(@"ivars_%@",ivarNameArray);
    }];
    
    UILabel *placeHolderLabel = [[UILabel alloc] init];
    placeHolderLabel.text = @"我是PlaceHolder，不是伍丽娟。";
    placeHolderLabel.numberOfLines = 0;
    placeHolderLabel.textColor = [UIColor lightGrayColor];
    [placeHolderLabel sizeToFit];
    placeHolderLabel.font = self.textView.font;
    placeHolderLabel.frame = CGRectMake(0, 0, 200, 100);
    [self.textView addSubview:placeHolderLabel];
    [self.textView setValue:placeHolderLabel forKey:@"_placeholderLabel"];
    
    NSLog(@"=====");
    [NSObject getAllPropertyNameWithClass:[UITextView class] Completed:^(NSArray *propertyNameArray) {
        NSLog(@"propertys_%@",propertyNameArray);
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
