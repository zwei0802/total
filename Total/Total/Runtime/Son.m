//
//  Son.m
//  Total
//
//  Created by xin on 2017/7/7.
//  Copyright © 2017年 elephants. All rights reserved.
//

#import "Son.h"

@implementation Son

- (id)init
{
    self = [super init];
    if (self) {
        NSLog(@"[%@]", NSStringFromClass([self class]));
        NSLog(@"[%@]", NSStringFromClass([super class]));
    }
    return self;
}

@end
