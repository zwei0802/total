//
//  Person+Associated.h
//  Total
//
//  Created by xin on 2017/7/8.
//  Copyright © 2017年 elephants. All rights reserved.
//

#import "Person.h"

@interface Person (Associated)

@property (strong, nonatomic) NSString * title;
@property (assign, nonatomic) NSInteger age;
@property (assign, nonatomic) BOOL sex;

@end
