//
//  NSObject+RunTime.h
//  Total
//
//  Created by xin on 2017/7/7.
//  Copyright © 2017年 elephants. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (RunTime)

#pragma mark - objc_msgSend (限制五个个参数及以内)
/**
 *实例方法
 */
+(id)msgSendToObj:(id)obj Selector:(SEL)selector Prarms:(NSArray*)params NeedReturn:(BOOL)needReturn;
/**
 *类方法
 */
+(id)msgSendToClass:(Class)YSClass Selector:(SEL)selector Prarms:(NSArray*)params NeedReturn:(BOOL)needReturn;

#pragma mark - NSInvocation (不限参数)
+(id)msgSendToObj_invocation:(id)obj Selector:(SEL)selector Prarms:(NSArray *)params NeedReturn:(BOOL)needReturn;

+ (id)msgSendToClass_invocation:(Class)YSClass Selector:(SEL)selector Prarms:(NSArray*)params NeedReturn:(BOOL)needReturn;

#pragma mark - 查看类的私有方法
/**
 *获取当前类的所有实例变量
 */
+(void)getAllIvarNameWithClass:(Class)YSClass Completed:(void (^)(NSArray *ivarNameArray))completed;

/**
 *获取当前类的所有属性
 */
+(void)getAllPropertyNameWithClass:(Class)YSClass Completed:(void (^)(NSArray *propertyNameArray))completed;

@end
