//
//  AppDelegate.h
//  Total
//
//  Created by xin on 2017/6/27.
//  Copyright © 2017年 elephants. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^LTBackgroundSessionCompletionHandlerBlock)();

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow * _Nullable window;

@property (nonatomic, copy, nullable) LTBackgroundSessionCompletionHandlerBlock backgroundSessionCompletionHandler;


@end

