//
//  MenuTableViewController.m
//  Total
//
//  Created by xin on 2017/6/27.
//  Copyright © 2017年 elephants. All rights reserved.
//

#import "MenuTableViewController.h"

@interface MenuTableViewController ()
{
    NSArray * demoArray;
    NSArray * controllerArray;
}
@end

@implementation MenuTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    demoArray = @[@"多线程NSOperation/GCD",
                  @"国际化语言",
                  @"NSRunloop",
                  @"截屏处理",
                  @"runtime",
                  @"断点续传，后台下载",
                  @"SDWebImage的原理,实现机制",
                  @"block和代理的，通知的区别。block的用法需要注意些什么",
                  @"strong，weak，retain，assign，copy nomatic 等的区别",
                  @"单利的写法。在单利中创建数组应该注意些什么",
                  @"NSString 的时候用copy和strong的区别",
                  @"响应值链",
                  @"UIScrollView和NSTimer组合做循环广告图轮播的时候有一个属性可以控制当上下滚动tableview的时候广告轮播图依然正常滚动",
                  @"Xcode最新的自动布局",
                  @"Git ，和svn的用法",
                  @"友盟报错可以查到具体某一行的错误，原理是什么",
                  @"Instrument  可以检测 电池的耗电量、和内存的消耗。的用法",
                  @"动画CABaseAnimation CAKeyAni….  CATrans…..  CAGoup….    等熟悉",
                  @"ARC的原理",
                  @"自定义控件"];
    
    controllerArray = @[@"LTGCDViewController",
                        @"LocalizationController",
                        @"LTNSRunloopViewController",
                        @"LTScreenShotViewController",
                        @"LTRunTimeViewController",
                        @"LTDownloadViewController"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return demoArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    cell.textLabel.text = demoArray[indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (controllerArray.count <= indexPath.row) {
        return;
    }
    
    NSString * controllerName = controllerArray[indexPath.row];
    
    UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:controllerName];
    
    [self.navigationController pushViewController:controller animated:YES];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
