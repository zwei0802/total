//
//  LTNSRunloopViewController.h
//  Total
//
//  Created by xin on 2017/7/3.
//  Copyright © 2017年 elephants. All rights reserved.

#import <UIKit/UIKit.h>

@interface LTNSRunloopViewController : UIViewController

@end

/*
 timer的注意事项
 1.在有scrollview的视图中，timer需要切换到别的runloop中，因为滑动的时候runloop会切换到UITrackingRunLoopMode模式,需要把timer添加到NSRunLoopCommonModes的自定义模式，
 NSURLConnection也是同理。
 2.对于cell的倒计时更新，需要使用通知或者其它方式来进行，遍历所有倒计时的时间进行每秒的刷新。
 */
