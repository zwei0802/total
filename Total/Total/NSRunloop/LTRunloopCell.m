//
//  LTRunloopCell.m
//  Total
//
//  Created by xin on 2017/7/4.
//  Copyright © 2017年 elephants. All rights reserved.
//

#import "LTRunloopCell.h"
#import "LTTimeModel.h"

@implementation LTRunloopCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(notificationCenterEvent:)
                                                 name:NSNotificationRunloopCell
                                               object:nil];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(LTTimeModel *)model
{
    _model = model;
    
    _numberLabel.text = model.title;
    _timeLabel.text = [model currentTimeString];
}

- (void)notificationCenterEvent:(NSNotification *)notification{
    
    _timeLabel.text = [self.model currentTimeString];
}

- (void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NSNotificationRunloopCell object:nil];
}

@end
