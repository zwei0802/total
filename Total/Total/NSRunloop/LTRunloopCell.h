//
//  LTRunloopCell.h
//  Total
//
//  Created by xin on 2017/7/4.
//  Copyright © 2017年 elephants. All rights reserved.
//

#import <UIKit/UIKit.h>
@class LTTimeModel;

static NSString  *NSNotificationRunloopCell = @"NSNotificationRunloopCell";

@interface LTRunloopCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *numberLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property (strong, nonatomic) LTTimeModel * model;

@end
