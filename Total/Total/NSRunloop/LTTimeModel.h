//
//  LTTimeModel.h
//  Total
//
//  Created by xin on 2017/7/6.
//  Copyright © 2017年 elephants. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LTTimeModel : NSObject

@property (nonatomic, strong) NSString        *title;
@property (nonatomic, strong) NSNumber        *countdownTime;


/**
 便利构造器

 @param title 标题
 @param countdownTime 倒计时
 @return 实例对象
 */
+ (instancetype)ltTimeModelWithTitle:(NSString *)title countdownTime:(NSNumber *)countdownTime;


/**
 转变countdownTime信息为字符串

 @return 返回字符串
 */
- (NSString *)currentTimeString;
/**
 计数减1
 */
- (void)countDown;

@end
