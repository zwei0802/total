//
//  LTTimeModel.m
//  Total
//
//  Created by xin on 2017/7/6.
//  Copyright © 2017年 elephants. All rights reserved.
//

#import "LTTimeModel.h"

@implementation LTTimeModel

+ (instancetype)ltTimeModelWithTitle:(NSString *)title countdownTime:(NSNumber *)countdownTime
{
    LTTimeModel * model = [[[self class] alloc] init];
    model.title         = title;
    model.countdownTime = countdownTime;
    return model;
}

- (NSString *)currentTimeString{
    
    NSInteger seconds = _countdownTime.integerValue;
    
    if (seconds <= 0) {
        
        return @"00:00:00";
        
    }else{
        
        return [NSString stringWithFormat:@"%02ld:%02ld:%02ld",(long)(seconds/3600),(long)(seconds%3600/60),(long)(seconds%60)];
    }
    
}

- (void)countDown{
    _countdownTime = @(_countdownTime.integerValue - 1);
}

@end
